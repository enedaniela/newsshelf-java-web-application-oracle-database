/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author oracle
 */
@Entity
@Table(name = "NEWS_CATEGORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NewsCategory.findAll", query = "SELECT n FROM NewsCategory n"),
    @NamedQuery(name = "NewsCategory.findByCategoryId", query = "SELECT n FROM NewsCategory n WHERE n.categoryId = :categoryId"),
    @NamedQuery(name = "NewsCategory.findByDescription", query = "SELECT n FROM NewsCategory n WHERE n.description = :description"),
    @NamedQuery(name = "NewsCategory.findByCreateDate", query = "SELECT n FROM NewsCategory n WHERE n.createDate = :createDate")})
public class NewsCategory implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CATEGORY_ID")
    private BigDecimal categoryId;
    @Size(max = 100)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoryId")
    private Collection<News> newsCollection;
    @JoinColumn(name = "CATEGORY_STATUS_ID", referencedColumnName = "CATEGORY_STATUS_ID")
    @ManyToOne
    private CategoryStatus categoryStatusId;

    public NewsCategory() {
    }

    public NewsCategory(BigDecimal categoryId) {
        this.categoryId = categoryId;
    }

    public BigDecimal getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(BigDecimal categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @XmlTransient
    public Collection<News> getNewsCollection() {
        return newsCollection;
    }

    public void setNewsCollection(Collection<News> newsCollection) {
        this.newsCollection = newsCollection;
    }

    public CategoryStatus getCategoryStatusId() {
        return categoryStatusId;
    }

    public void setCategoryStatusId(CategoryStatus categoryStatusId) {
        this.categoryStatusId = categoryStatusId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (categoryId != null ? categoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NewsCategory)) {
            return false;
        }
        NewsCategory other = (NewsCategory) object;
        if ((this.categoryId == null && other.categoryId != null) || (this.categoryId != null && !this.categoryId.equals(other.categoryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NewsPack.NewsCategory[ categoryId=" + categoryId + " ]";
    }
    
}

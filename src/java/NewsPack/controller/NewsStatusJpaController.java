/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import NewsPack.News;
import NewsPack.NewsStatus;
import NewsPack.controller.exceptions.IllegalOrphanException;
import NewsPack.controller.exceptions.NonexistentEntityException;
import NewsPack.controller.exceptions.PreexistingEntityException;
import NewsPack.controller.exceptions.RollbackFailureException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author oracle
 */
public class NewsStatusJpaController implements Serializable {

    public NewsStatusJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(NewsStatus newsStatus) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (newsStatus.getNewsCollection() == null) {
            newsStatus.setNewsCollection(new ArrayList<News>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<News> attachedNewsCollection = new ArrayList<News>();
            for (News newsCollectionNewsToAttach : newsStatus.getNewsCollection()) {
                newsCollectionNewsToAttach = em.getReference(newsCollectionNewsToAttach.getClass(), newsCollectionNewsToAttach.getNewsId());
                attachedNewsCollection.add(newsCollectionNewsToAttach);
            }
            newsStatus.setNewsCollection(attachedNewsCollection);
            em.persist(newsStatus);
            for (News newsCollectionNews : newsStatus.getNewsCollection()) {
                NewsStatus oldNewsStatusIdOfNewsCollectionNews = newsCollectionNews.getNewsStatusId();
                newsCollectionNews.setNewsStatusId(newsStatus);
                newsCollectionNews = em.merge(newsCollectionNews);
                if (oldNewsStatusIdOfNewsCollectionNews != null) {
                    oldNewsStatusIdOfNewsCollectionNews.getNewsCollection().remove(newsCollectionNews);
                    oldNewsStatusIdOfNewsCollectionNews = em.merge(oldNewsStatusIdOfNewsCollectionNews);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findNewsStatus(newsStatus.getNewsStatusId()) != null) {
                throw new PreexistingEntityException("NewsStatus " + newsStatus + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(NewsStatus newsStatus) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            NewsStatus persistentNewsStatus = em.find(NewsStatus.class, newsStatus.getNewsStatusId());
            Collection<News> newsCollectionOld = persistentNewsStatus.getNewsCollection();
            Collection<News> newsCollectionNew = newsStatus.getNewsCollection();
            List<String> illegalOrphanMessages = null;
            for (News newsCollectionOldNews : newsCollectionOld) {
                if (!newsCollectionNew.contains(newsCollectionOldNews)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain News " + newsCollectionOldNews + " since its newsStatusId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<News> attachedNewsCollectionNew = new ArrayList<News>();
            for (News newsCollectionNewNewsToAttach : newsCollectionNew) {
                newsCollectionNewNewsToAttach = em.getReference(newsCollectionNewNewsToAttach.getClass(), newsCollectionNewNewsToAttach.getNewsId());
                attachedNewsCollectionNew.add(newsCollectionNewNewsToAttach);
            }
            newsCollectionNew = attachedNewsCollectionNew;
            newsStatus.setNewsCollection(newsCollectionNew);
            newsStatus = em.merge(newsStatus);
            for (News newsCollectionNewNews : newsCollectionNew) {
                if (!newsCollectionOld.contains(newsCollectionNewNews)) {
                    NewsStatus oldNewsStatusIdOfNewsCollectionNewNews = newsCollectionNewNews.getNewsStatusId();
                    newsCollectionNewNews.setNewsStatusId(newsStatus);
                    newsCollectionNewNews = em.merge(newsCollectionNewNews);
                    if (oldNewsStatusIdOfNewsCollectionNewNews != null && !oldNewsStatusIdOfNewsCollectionNewNews.equals(newsStatus)) {
                        oldNewsStatusIdOfNewsCollectionNewNews.getNewsCollection().remove(newsCollectionNewNews);
                        oldNewsStatusIdOfNewsCollectionNewNews = em.merge(oldNewsStatusIdOfNewsCollectionNewNews);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigDecimal id = newsStatus.getNewsStatusId();
                if (findNewsStatus(id) == null) {
                    throw new NonexistentEntityException("The newsStatus with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigDecimal id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            NewsStatus newsStatus;
            try {
                newsStatus = em.getReference(NewsStatus.class, id);
                newsStatus.getNewsStatusId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The newsStatus with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<News> newsCollectionOrphanCheck = newsStatus.getNewsCollection();
            for (News newsCollectionOrphanCheckNews : newsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This NewsStatus (" + newsStatus + ") cannot be destroyed since the News " + newsCollectionOrphanCheckNews + " in its newsCollection field has a non-nullable newsStatusId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(newsStatus);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NewsStatus> findNewsStatusEntities() {
        return findNewsStatusEntities(true, -1, -1);
    }

    public List<NewsStatus> findNewsStatusEntities(int maxResults, int firstResult) {
        return findNewsStatusEntities(false, maxResults, firstResult);
    }

    private List<NewsStatus> findNewsStatusEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from NewsStatus as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public NewsStatus findNewsStatus(BigDecimal id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NewsStatus.class, id);
        } finally {
            em.close();
        }
    }

    public int getNewsStatusCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from NewsStatus as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author oracle
 */
@Entity
@Table(name = "CATEGORY_STATUS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategoryStatus.findAll", query = "SELECT c FROM CategoryStatus c"),
    @NamedQuery(name = "CategoryStatus.findByCategoryStatusId", query = "SELECT c FROM CategoryStatus c WHERE c.categoryStatusId = :categoryStatusId"),
    @NamedQuery(name = "CategoryStatus.findByDescription", query = "SELECT c FROM CategoryStatus c WHERE c.description = :description"),
    @NamedQuery(name = "CategoryStatus.findByCreateDate", query = "SELECT c FROM CategoryStatus c WHERE c.createDate = :createDate")})
public class CategoryStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CATEGORY_STATUS_ID")
    private BigDecimal categoryStatusId;
    @Size(max = 100)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @OneToMany(mappedBy = "categoryStatusId")
    private Collection<NewsCategory> newsCategoryCollection;

    public CategoryStatus() {
    }

    public CategoryStatus(BigDecimal categoryStatusId) {
        this.categoryStatusId = categoryStatusId;
    }

    public BigDecimal getCategoryStatusId() {
        return categoryStatusId;
    }

    public void setCategoryStatusId(BigDecimal categoryStatusId) {
        this.categoryStatusId = categoryStatusId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @XmlTransient
    public Collection<NewsCategory> getNewsCategoryCollection() {
        return newsCategoryCollection;
    }

    public void setNewsCategoryCollection(Collection<NewsCategory> newsCategoryCollection) {
        this.newsCategoryCollection = newsCategoryCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (categoryStatusId != null ? categoryStatusId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoryStatus)) {
            return false;
        }
        CategoryStatus other = (CategoryStatus) object;
        if ((this.categoryStatusId == null && other.categoryStatusId != null) || (this.categoryStatusId != null && !this.categoryStatusId.equals(other.categoryStatusId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NewsPack.CategoryStatus[ categoryStatusId=" + categoryStatusId + " ]";
    }
    
}

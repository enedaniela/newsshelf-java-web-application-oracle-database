/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author oracle
 */
@Entity
@Table(name = "NEWS_IMAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NewsImage.findAll", query = "SELECT n FROM NewsImage n"),
    @NamedQuery(name = "NewsImage.findByImageId", query = "SELECT n FROM NewsImage n WHERE n.imageId = :imageId"),
    @NamedQuery(name = "NewsImage.findByTitle", query = "SELECT n FROM NewsImage n WHERE n.title = :title")})
public class NewsImage implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IMAGE_ID")
    private BigDecimal imageId;
    @Lob
    @Column(name = "IMAGE")
    private Serializable image;
    @Size(max = 200)
    @Column(name = "TITLE")
    private String title;
    @OneToMany(mappedBy = "imageId")
    private Collection<NewsImageAssoc> newsImageAssocCollection;

    public NewsImage() {
    }

    public NewsImage(BigDecimal imageId) {
        this.imageId = imageId;
    }

    public BigDecimal getImageId() {
        return imageId;
    }

    public void setImageId(BigDecimal imageId) {
        this.imageId = imageId;
    }

    public Serializable getImage() {
        return image;
    }

    public void setImage(Serializable image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @XmlTransient
    public Collection<NewsImageAssoc> getNewsImageAssocCollection() {
        return newsImageAssocCollection;
    }

    public void setNewsImageAssocCollection(Collection<NewsImageAssoc> newsImageAssocCollection) {
        this.newsImageAssocCollection = newsImageAssocCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (imageId != null ? imageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NewsImage)) {
            return false;
        }
        NewsImage other = (NewsImage) object;
        if ((this.imageId == null && other.imageId != null) || (this.imageId != null && !this.imageId.equals(other.imageId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NewsPack.NewsImage[ imageId=" + imageId + " ]";
    }
    
}

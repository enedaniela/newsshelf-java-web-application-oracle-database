/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author oracle
 */
@Entity
@Table(name = "COMMENT_STATUS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CommentStatus.findAll", query = "SELECT c FROM CommentStatus c"),
    @NamedQuery(name = "CommentStatus.findByCommentStatusId", query = "SELECT c FROM CommentStatus c WHERE c.commentStatusId = :commentStatusId"),
    @NamedQuery(name = "CommentStatus.findByDescription", query = "SELECT c FROM CommentStatus c WHERE c.description = :description"),
    @NamedQuery(name = "CommentStatus.findByCreateDate", query = "SELECT c FROM CommentStatus c WHERE c.createDate = :createDate")})
public class CommentStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMMENT_STATUS_ID")
    private BigDecimal commentStatusId;
    @Size(max = 100)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @OneToMany(mappedBy = "commentStatusId")
    private Collection<Comments> commentsCollection;

    public CommentStatus() {
    }

    public CommentStatus(BigDecimal commentStatusId) {
        this.commentStatusId = commentStatusId;
    }

    public BigDecimal getCommentStatusId() {
        return commentStatusId;
    }

    public void setCommentStatusId(BigDecimal commentStatusId) {
        this.commentStatusId = commentStatusId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @XmlTransient
    public Collection<Comments> getCommentsCollection() {
        return commentsCollection;
    }

    public void setCommentsCollection(Collection<Comments> commentsCollection) {
        this.commentsCollection = commentsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (commentStatusId != null ? commentStatusId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CommentStatus)) {
            return false;
        }
        CommentStatus other = (CommentStatus) object;
        if ((this.commentStatusId == null && other.commentStatusId != null) || (this.commentStatusId != null && !this.commentStatusId.equals(other.commentStatusId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NewsPack.CommentStatus[ commentStatusId=" + commentStatusId + " ]";
    }
    
}

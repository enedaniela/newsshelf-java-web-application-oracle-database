/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import NewsPack.Comments;
import NewsPack.UsersComments;
import NewsPack.controller.exceptions.NonexistentEntityException;
import NewsPack.controller.exceptions.PreexistingEntityException;
import NewsPack.controller.exceptions.RollbackFailureException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author oracle
 */
public class UsersCommentsJpaController implements Serializable {

    public UsersCommentsJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UsersComments usersComments) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (usersComments.getCommentsCollection() == null) {
            usersComments.setCommentsCollection(new ArrayList<Comments>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<Comments> attachedCommentsCollection = new ArrayList<Comments>();
            for (Comments commentsCollectionCommentsToAttach : usersComments.getCommentsCollection()) {
                commentsCollectionCommentsToAttach = em.getReference(commentsCollectionCommentsToAttach.getClass(), commentsCollectionCommentsToAttach.getCommentId());
                attachedCommentsCollection.add(commentsCollectionCommentsToAttach);
            }
            usersComments.setCommentsCollection(attachedCommentsCollection);
            em.persist(usersComments);
            for (Comments commentsCollectionComments : usersComments.getCommentsCollection()) {
                UsersComments oldUserIdOfCommentsCollectionComments = commentsCollectionComments.getUserId();
                commentsCollectionComments.setUserId(usersComments);
                commentsCollectionComments = em.merge(commentsCollectionComments);
                if (oldUserIdOfCommentsCollectionComments != null) {
                    oldUserIdOfCommentsCollectionComments.getCommentsCollection().remove(commentsCollectionComments);
                    oldUserIdOfCommentsCollectionComments = em.merge(oldUserIdOfCommentsCollectionComments);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findUsersComments(usersComments.getUserId()) != null) {
                throw new PreexistingEntityException("UsersComments " + usersComments + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UsersComments usersComments) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            UsersComments persistentUsersComments = em.find(UsersComments.class, usersComments.getUserId());
            Collection<Comments> commentsCollectionOld = persistentUsersComments.getCommentsCollection();
            Collection<Comments> commentsCollectionNew = usersComments.getCommentsCollection();
            Collection<Comments> attachedCommentsCollectionNew = new ArrayList<Comments>();
            for (Comments commentsCollectionNewCommentsToAttach : commentsCollectionNew) {
                commentsCollectionNewCommentsToAttach = em.getReference(commentsCollectionNewCommentsToAttach.getClass(), commentsCollectionNewCommentsToAttach.getCommentId());
                attachedCommentsCollectionNew.add(commentsCollectionNewCommentsToAttach);
            }
            commentsCollectionNew = attachedCommentsCollectionNew;
            usersComments.setCommentsCollection(commentsCollectionNew);
            usersComments = em.merge(usersComments);
            for (Comments commentsCollectionOldComments : commentsCollectionOld) {
                if (!commentsCollectionNew.contains(commentsCollectionOldComments)) {
                    commentsCollectionOldComments.setUserId(null);
                    commentsCollectionOldComments = em.merge(commentsCollectionOldComments);
                }
            }
            for (Comments commentsCollectionNewComments : commentsCollectionNew) {
                if (!commentsCollectionOld.contains(commentsCollectionNewComments)) {
                    UsersComments oldUserIdOfCommentsCollectionNewComments = commentsCollectionNewComments.getUserId();
                    commentsCollectionNewComments.setUserId(usersComments);
                    commentsCollectionNewComments = em.merge(commentsCollectionNewComments);
                    if (oldUserIdOfCommentsCollectionNewComments != null && !oldUserIdOfCommentsCollectionNewComments.equals(usersComments)) {
                        oldUserIdOfCommentsCollectionNewComments.getCommentsCollection().remove(commentsCollectionNewComments);
                        oldUserIdOfCommentsCollectionNewComments = em.merge(oldUserIdOfCommentsCollectionNewComments);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigDecimal id = usersComments.getUserId();
                if (findUsersComments(id) == null) {
                    throw new NonexistentEntityException("The usersComments with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigDecimal id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            UsersComments usersComments;
            try {
                usersComments = em.getReference(UsersComments.class, id);
                usersComments.getUserId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The usersComments with id " + id + " no longer exists.", enfe);
            }
            Collection<Comments> commentsCollection = usersComments.getCommentsCollection();
            for (Comments commentsCollectionComments : commentsCollection) {
                commentsCollectionComments.setUserId(null);
                commentsCollectionComments = em.merge(commentsCollectionComments);
            }
            em.remove(usersComments);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UsersComments> findUsersCommentsEntities() {
        return findUsersCommentsEntities(true, -1, -1);
    }

    public List<UsersComments> findUsersCommentsEntities(int maxResults, int firstResult) {
        return findUsersCommentsEntities(false, maxResults, firstResult);
    }

    private List<UsersComments> findUsersCommentsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from UsersComments as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UsersComments findUsersComments(BigDecimal id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UsersComments.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsersCommentsCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from UsersComments as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

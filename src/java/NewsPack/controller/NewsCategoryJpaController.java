/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import NewsPack.CategoryStatus;
import NewsPack.News;
import NewsPack.NewsCategory;
import NewsPack.controller.exceptions.IllegalOrphanException;
import NewsPack.controller.exceptions.NonexistentEntityException;
import NewsPack.controller.exceptions.PreexistingEntityException;
import NewsPack.controller.exceptions.RollbackFailureException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author oracle
 */
public class NewsCategoryJpaController implements Serializable {

    public NewsCategoryJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(NewsCategory newsCategory) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (newsCategory.getNewsCollection() == null) {
            newsCategory.setNewsCollection(new ArrayList<News>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            CategoryStatus categoryStatusId = newsCategory.getCategoryStatusId();
            if (categoryStatusId != null) {
                categoryStatusId = em.getReference(categoryStatusId.getClass(), categoryStatusId.getCategoryStatusId());
                newsCategory.setCategoryStatusId(categoryStatusId);
            }
            Collection<News> attachedNewsCollection = new ArrayList<News>();
            for (News newsCollectionNewsToAttach : newsCategory.getNewsCollection()) {
                newsCollectionNewsToAttach = em.getReference(newsCollectionNewsToAttach.getClass(), newsCollectionNewsToAttach.getNewsId());
                attachedNewsCollection.add(newsCollectionNewsToAttach);
            }
            newsCategory.setNewsCollection(attachedNewsCollection);
            em.persist(newsCategory);
            if (categoryStatusId != null) {
                categoryStatusId.getNewsCategoryCollection().add(newsCategory);
                categoryStatusId = em.merge(categoryStatusId);
            }
            for (News newsCollectionNews : newsCategory.getNewsCollection()) {
                NewsCategory oldCategoryIdOfNewsCollectionNews = newsCollectionNews.getCategoryId();
                newsCollectionNews.setCategoryId(newsCategory);
                newsCollectionNews = em.merge(newsCollectionNews);
                if (oldCategoryIdOfNewsCollectionNews != null) {
                    oldCategoryIdOfNewsCollectionNews.getNewsCollection().remove(newsCollectionNews);
                    oldCategoryIdOfNewsCollectionNews = em.merge(oldCategoryIdOfNewsCollectionNews);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findNewsCategory(newsCategory.getCategoryId()) != null) {
                throw new PreexistingEntityException("NewsCategory " + newsCategory + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(NewsCategory newsCategory) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            NewsCategory persistentNewsCategory = em.find(NewsCategory.class, newsCategory.getCategoryId());
            CategoryStatus categoryStatusIdOld = persistentNewsCategory.getCategoryStatusId();
            CategoryStatus categoryStatusIdNew = newsCategory.getCategoryStatusId();
            Collection<News> newsCollectionOld = persistentNewsCategory.getNewsCollection();
            Collection<News> newsCollectionNew = newsCategory.getNewsCollection();
            List<String> illegalOrphanMessages = null;
            for (News newsCollectionOldNews : newsCollectionOld) {
                if (!newsCollectionNew.contains(newsCollectionOldNews)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain News " + newsCollectionOldNews + " since its categoryId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (categoryStatusIdNew != null) {
                categoryStatusIdNew = em.getReference(categoryStatusIdNew.getClass(), categoryStatusIdNew.getCategoryStatusId());
                newsCategory.setCategoryStatusId(categoryStatusIdNew);
            }
            Collection<News> attachedNewsCollectionNew = new ArrayList<News>();
            for (News newsCollectionNewNewsToAttach : newsCollectionNew) {
                newsCollectionNewNewsToAttach = em.getReference(newsCollectionNewNewsToAttach.getClass(), newsCollectionNewNewsToAttach.getNewsId());
                attachedNewsCollectionNew.add(newsCollectionNewNewsToAttach);
            }
            newsCollectionNew = attachedNewsCollectionNew;
            newsCategory.setNewsCollection(newsCollectionNew);
            newsCategory = em.merge(newsCategory);
            if (categoryStatusIdOld != null && !categoryStatusIdOld.equals(categoryStatusIdNew)) {
                categoryStatusIdOld.getNewsCategoryCollection().remove(newsCategory);
                categoryStatusIdOld = em.merge(categoryStatusIdOld);
            }
            if (categoryStatusIdNew != null && !categoryStatusIdNew.equals(categoryStatusIdOld)) {
                categoryStatusIdNew.getNewsCategoryCollection().add(newsCategory);
                categoryStatusIdNew = em.merge(categoryStatusIdNew);
            }
            for (News newsCollectionNewNews : newsCollectionNew) {
                if (!newsCollectionOld.contains(newsCollectionNewNews)) {
                    NewsCategory oldCategoryIdOfNewsCollectionNewNews = newsCollectionNewNews.getCategoryId();
                    newsCollectionNewNews.setCategoryId(newsCategory);
                    newsCollectionNewNews = em.merge(newsCollectionNewNews);
                    if (oldCategoryIdOfNewsCollectionNewNews != null && !oldCategoryIdOfNewsCollectionNewNews.equals(newsCategory)) {
                        oldCategoryIdOfNewsCollectionNewNews.getNewsCollection().remove(newsCollectionNewNews);
                        oldCategoryIdOfNewsCollectionNewNews = em.merge(oldCategoryIdOfNewsCollectionNewNews);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigDecimal id = newsCategory.getCategoryId();
                if (findNewsCategory(id) == null) {
                    throw new NonexistentEntityException("The newsCategory with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigDecimal id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            NewsCategory newsCategory;
            try {
                newsCategory = em.getReference(NewsCategory.class, id);
                newsCategory.getCategoryId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The newsCategory with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<News> newsCollectionOrphanCheck = newsCategory.getNewsCollection();
            for (News newsCollectionOrphanCheckNews : newsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This NewsCategory (" + newsCategory + ") cannot be destroyed since the News " + newsCollectionOrphanCheckNews + " in its newsCollection field has a non-nullable categoryId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            CategoryStatus categoryStatusId = newsCategory.getCategoryStatusId();
            if (categoryStatusId != null) {
                categoryStatusId.getNewsCategoryCollection().remove(newsCategory);
                categoryStatusId = em.merge(categoryStatusId);
            }
            em.remove(newsCategory);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NewsCategory> findNewsCategoryEntities() {
        return findNewsCategoryEntities(true, -1, -1);
    }

    public List<NewsCategory> findNewsCategoryEntities(int maxResults, int firstResult) {
        return findNewsCategoryEntities(false, maxResults, firstResult);
    }

    private List<NewsCategory> findNewsCategoryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from NewsCategory as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public NewsCategory findNewsCategory(BigDecimal id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NewsCategory.class, id);
        } finally {
            em.close();
        }
    }

    public int getNewsCategoryCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from NewsCategory as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

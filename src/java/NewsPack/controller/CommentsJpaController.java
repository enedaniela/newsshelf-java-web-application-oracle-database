/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import NewsPack.UsersComments;
import NewsPack.News;
import NewsPack.CommentStatus;
import NewsPack.Comments;
import NewsPack.controller.exceptions.NonexistentEntityException;
import NewsPack.controller.exceptions.PreexistingEntityException;
import NewsPack.controller.exceptions.RollbackFailureException;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author oracle
 */
public class CommentsJpaController implements Serializable {

    public CommentsJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Comments comments) throws PreexistingEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            UsersComments userId = comments.getUserId();
            if (userId != null) {
                userId = em.getReference(userId.getClass(), userId.getUserId());
                comments.setUserId(userId);
            }
            News newsId = comments.getNewsId();
            if (newsId != null) {
                newsId = em.getReference(newsId.getClass(), newsId.getNewsId());
                comments.setNewsId(newsId);
            }
            CommentStatus commentStatusId = comments.getCommentStatusId();
            if (commentStatusId != null) {
                commentStatusId = em.getReference(commentStatusId.getClass(), commentStatusId.getCommentStatusId());
                comments.setCommentStatusId(commentStatusId);
            }
            em.persist(comments);
            if (userId != null) {
                userId.getCommentsCollection().add(comments);
                userId = em.merge(userId);
            }
            if (newsId != null) {
                newsId.getCommentsCollection().add(comments);
                newsId = em.merge(newsId);
            }
            if (commentStatusId != null) {
                commentStatusId.getCommentsCollection().add(comments);
                commentStatusId = em.merge(commentStatusId);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findComments(comments.getCommentId()) != null) {
                throw new PreexistingEntityException("Comments " + comments + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Comments comments) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Comments persistentComments = em.find(Comments.class, comments.getCommentId());
            UsersComments userIdOld = persistentComments.getUserId();
            UsersComments userIdNew = comments.getUserId();
            News newsIdOld = persistentComments.getNewsId();
            News newsIdNew = comments.getNewsId();
            CommentStatus commentStatusIdOld = persistentComments.getCommentStatusId();
            CommentStatus commentStatusIdNew = comments.getCommentStatusId();
            if (userIdNew != null) {
                userIdNew = em.getReference(userIdNew.getClass(), userIdNew.getUserId());
                comments.setUserId(userIdNew);
            }
            if (newsIdNew != null) {
                newsIdNew = em.getReference(newsIdNew.getClass(), newsIdNew.getNewsId());
                comments.setNewsId(newsIdNew);
            }
            if (commentStatusIdNew != null) {
                commentStatusIdNew = em.getReference(commentStatusIdNew.getClass(), commentStatusIdNew.getCommentStatusId());
                comments.setCommentStatusId(commentStatusIdNew);
            }
            comments = em.merge(comments);
            if (userIdOld != null && !userIdOld.equals(userIdNew)) {
                userIdOld.getCommentsCollection().remove(comments);
                userIdOld = em.merge(userIdOld);
            }
            if (userIdNew != null && !userIdNew.equals(userIdOld)) {
                userIdNew.getCommentsCollection().add(comments);
                userIdNew = em.merge(userIdNew);
            }
            if (newsIdOld != null && !newsIdOld.equals(newsIdNew)) {
                newsIdOld.getCommentsCollection().remove(comments);
                newsIdOld = em.merge(newsIdOld);
            }
            if (newsIdNew != null && !newsIdNew.equals(newsIdOld)) {
                newsIdNew.getCommentsCollection().add(comments);
                newsIdNew = em.merge(newsIdNew);
            }
            if (commentStatusIdOld != null && !commentStatusIdOld.equals(commentStatusIdNew)) {
                commentStatusIdOld.getCommentsCollection().remove(comments);
                commentStatusIdOld = em.merge(commentStatusIdOld);
            }
            if (commentStatusIdNew != null && !commentStatusIdNew.equals(commentStatusIdOld)) {
                commentStatusIdNew.getCommentsCollection().add(comments);
                commentStatusIdNew = em.merge(commentStatusIdNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigDecimal id = comments.getCommentId();
                if (findComments(id) == null) {
                    throw new NonexistentEntityException("The comments with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigDecimal id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Comments comments;
            try {
                comments = em.getReference(Comments.class, id);
                comments.getCommentId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The comments with id " + id + " no longer exists.", enfe);
            }
            UsersComments userId = comments.getUserId();
            if (userId != null) {
                userId.getCommentsCollection().remove(comments);
                userId = em.merge(userId);
            }
            News newsId = comments.getNewsId();
            if (newsId != null) {
                newsId.getCommentsCollection().remove(comments);
                newsId = em.merge(newsId);
            }
            CommentStatus commentStatusId = comments.getCommentStatusId();
            if (commentStatusId != null) {
                commentStatusId.getCommentsCollection().remove(comments);
                commentStatusId = em.merge(commentStatusId);
            }
            em.remove(comments);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Comments> findCommentsEntities() {
        return findCommentsEntities(true, -1, -1);
    }

    public List<Comments> findCommentsEntities(int maxResults, int firstResult) {
        return findCommentsEntities(false, maxResults, firstResult);
    }

    private List<Comments> findCommentsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Comments as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Comments findComments(BigDecimal id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Comments.class, id);
        } finally {
            em.close();
        }
    }

    public int getCommentsCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Comments as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

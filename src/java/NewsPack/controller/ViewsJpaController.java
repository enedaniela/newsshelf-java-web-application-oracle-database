/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import NewsPack.News;
import NewsPack.Views;
import NewsPack.controller.exceptions.NonexistentEntityException;
import NewsPack.controller.exceptions.PreexistingEntityException;
import NewsPack.controller.exceptions.RollbackFailureException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author oracle
 */
public class ViewsJpaController implements Serializable {

    public ViewsJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Views views) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (views.getNewsCollection() == null) {
            views.setNewsCollection(new ArrayList<News>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<News> attachedNewsCollection = new ArrayList<News>();
            for (News newsCollectionNewsToAttach : views.getNewsCollection()) {
                newsCollectionNewsToAttach = em.getReference(newsCollectionNewsToAttach.getClass(), newsCollectionNewsToAttach.getNewsId());
                attachedNewsCollection.add(newsCollectionNewsToAttach);
            }
            views.setNewsCollection(attachedNewsCollection);
            em.persist(views);
            for (News newsCollectionNews : views.getNewsCollection()) {
                Views oldViewIdOfNewsCollectionNews = newsCollectionNews.getViewId();
                newsCollectionNews.setViewId(views);
                newsCollectionNews = em.merge(newsCollectionNews);
                if (oldViewIdOfNewsCollectionNews != null) {
                    oldViewIdOfNewsCollectionNews.getNewsCollection().remove(newsCollectionNews);
                    oldViewIdOfNewsCollectionNews = em.merge(oldViewIdOfNewsCollectionNews);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findViews(views.getViewId()) != null) {
                throw new PreexistingEntityException("Views " + views + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Views views) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Views persistentViews = em.find(Views.class, views.getViewId());
            Collection<News> newsCollectionOld = persistentViews.getNewsCollection();
            Collection<News> newsCollectionNew = views.getNewsCollection();
            Collection<News> attachedNewsCollectionNew = new ArrayList<News>();
            for (News newsCollectionNewNewsToAttach : newsCollectionNew) {
                newsCollectionNewNewsToAttach = em.getReference(newsCollectionNewNewsToAttach.getClass(), newsCollectionNewNewsToAttach.getNewsId());
                attachedNewsCollectionNew.add(newsCollectionNewNewsToAttach);
            }
            newsCollectionNew = attachedNewsCollectionNew;
            views.setNewsCollection(newsCollectionNew);
            views = em.merge(views);
            for (News newsCollectionOldNews : newsCollectionOld) {
                if (!newsCollectionNew.contains(newsCollectionOldNews)) {
                    newsCollectionOldNews.setViewId(null);
                    newsCollectionOldNews = em.merge(newsCollectionOldNews);
                }
            }
            for (News newsCollectionNewNews : newsCollectionNew) {
                if (!newsCollectionOld.contains(newsCollectionNewNews)) {
                    Views oldViewIdOfNewsCollectionNewNews = newsCollectionNewNews.getViewId();
                    newsCollectionNewNews.setViewId(views);
                    newsCollectionNewNews = em.merge(newsCollectionNewNews);
                    if (oldViewIdOfNewsCollectionNewNews != null && !oldViewIdOfNewsCollectionNewNews.equals(views)) {
                        oldViewIdOfNewsCollectionNewNews.getNewsCollection().remove(newsCollectionNewNews);
                        oldViewIdOfNewsCollectionNewNews = em.merge(oldViewIdOfNewsCollectionNewNews);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigDecimal id = views.getViewId();
                if (findViews(id) == null) {
                    throw new NonexistentEntityException("The views with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigDecimal id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Views views;
            try {
                views = em.getReference(Views.class, id);
                views.getViewId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The views with id " + id + " no longer exists.", enfe);
            }
            Collection<News> newsCollection = views.getNewsCollection();
            for (News newsCollectionNews : newsCollection) {
                newsCollectionNews.setViewId(null);
                newsCollectionNews = em.merge(newsCollectionNews);
            }
            em.remove(views);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Views> findViewsEntities() {
        return findViewsEntities(true, -1, -1);
    }

    public List<Views> findViewsEntities(int maxResults, int firstResult) {
        return findViewsEntities(false, maxResults, firstResult);
    }

    private List<Views> findViewsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Views as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Views findViews(BigDecimal id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Views.class, id);
        } finally {
            em.close();
        }
    }

    public int getViewsCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Views as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

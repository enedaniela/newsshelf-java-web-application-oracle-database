/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author oracle
 */
@Entity
@Table(name = "USERS_COMMENTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsersComments.findAll", query = "SELECT u FROM UsersComments u"),
    @NamedQuery(name = "UsersComments.findByUserId", query = "SELECT u FROM UsersComments u WHERE u.userId = :userId"),
    @NamedQuery(name = "UsersComments.findByUserName", query = "SELECT u FROM UsersComments u WHERE u.userName = :userName"),
    @NamedQuery(name = "UsersComments.findByUserEmail", query = "SELECT u FROM UsersComments u WHERE u.userEmail = :userEmail")})
public class UsersComments implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private BigDecimal userId;
    @Size(max = 200)
    @Column(name = "USER_NAME")
    private String userName;
    @Size(max = 254)
    @Column(name = "USER_EMAIL")
    private String userEmail;
    @OneToMany(mappedBy = "userId")
    private Collection<Comments> commentsCollection;

    public UsersComments() {
    }

    public UsersComments(BigDecimal userId) {
        this.userId = userId;
    }

    public BigDecimal getUserId() {
        return userId;
    }

    public void setUserId(BigDecimal userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @XmlTransient
    public Collection<Comments> getCommentsCollection() {
        return commentsCollection;
    }

    public void setCommentsCollection(Collection<Comments> commentsCollection) {
        this.commentsCollection = commentsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsersComments)) {
            return false;
        }
        UsersComments other = (UsersComments) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NewsPack.UsersComments[ userId=" + userId + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author oracle
 */
@Entity
@Table(name = "AUTHORS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Authors.findAll", query = "SELECT a FROM Authors a"),
    @NamedQuery(name = "Authors.findByAuthorId", query = "SELECT a FROM Authors a WHERE a.authorId = :authorId"),
    @NamedQuery(name = "Authors.findByUsername", query = "SELECT a FROM Authors a WHERE a.username = :username"),
    @NamedQuery(name = "Authors.findByPassword", query = "SELECT a FROM Authors a WHERE a.password = :password"),
    @NamedQuery(name = "Authors.findByFirstName", query = "SELECT a FROM Authors a WHERE a.firstName = :firstName"),
    @NamedQuery(name = "Authors.findBySecondName", query = "SELECT a FROM Authors a WHERE a.secondName = :secondName"),
    @NamedQuery(name = "Authors.findByAuthorDescription", query = "SELECT a FROM Authors a WHERE a.authorDescription = :authorDescription"),
    @NamedQuery(name = "Authors.findByEmail", query = "SELECT a FROM Authors a WHERE a.email = :email"),
    @NamedQuery(name = "Authors.findByAge", query = "SELECT a FROM Authors a WHERE a.age = :age")})
public class Authors implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AUTHOR_ID")
    private BigDecimal authorId;
    @Size(max = 20)
    @Column(name = "USERNAME")
    private String username;
    @Size(max = 50)
    @Column(name = "PASSWORD")
    private String password;
    @Size(max = 50)
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Size(max = 50)
    @Column(name = "SECOND_NAME")
    private String secondName;
    @Size(max = 2000)
    @Column(name = "AUTHOR_DESCRIPTION")
    private String authorDescription;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 50)
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "AGE")
    private BigInteger age;
    @JoinColumn(name = "AUTHOR_STATUS_ID", referencedColumnName = "AUTHOR_STATUS_ID")
    @ManyToOne
    private AuthorStatus authorStatusId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "authorId")
    private Collection<News> newsCollection;

    public Authors() {
    }

    public Authors(BigDecimal authorId) {
        this.authorId = authorId;
    }

    public BigDecimal getAuthorId() {
        return authorId;
    }

    public void setAuthorId(BigDecimal authorId) {
        this.authorId = authorId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getAuthorDescription() {
        return authorDescription;
    }

    public void setAuthorDescription(String authorDescription) {
        this.authorDescription = authorDescription;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigInteger getAge() {
        return age;
    }

    public void setAge(BigInteger age) {
        this.age = age;
    }

    public AuthorStatus getAuthorStatusId() {
        return authorStatusId;
    }

    public void setAuthorStatusId(AuthorStatus authorStatusId) {
        this.authorStatusId = authorStatusId;
    }

    @XmlTransient
    public Collection<News> getNewsCollection() {
        return newsCollection;
    }

    public void setNewsCollection(Collection<News> newsCollection) {
        this.newsCollection = newsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (authorId != null ? authorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Authors)) {
            return false;
        }
        Authors other = (Authors) object;
        if ((this.authorId == null && other.authorId != null) || (this.authorId != null && !this.authorId.equals(other.authorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NewsPack.Authors[ authorId=" + authorId + " ]";
    }
    
}

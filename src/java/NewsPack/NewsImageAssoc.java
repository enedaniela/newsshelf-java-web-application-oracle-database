/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author oracle
 */
@Entity
@Table(name = "NEWS_IMAGE_ASSOC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NewsImageAssoc.findAll", query = "SELECT n FROM NewsImageAssoc n"),
    @NamedQuery(name = "NewsImageAssoc.findByAssocId", query = "SELECT n FROM NewsImageAssoc n WHERE n.assocId = :assocId")})
public class NewsImageAssoc implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ASSOC_ID")
    private BigDecimal assocId;
    @JoinColumn(name = "IMAGE_ID", referencedColumnName = "IMAGE_ID")
    @ManyToOne
    private NewsImage imageId;
    @JoinColumn(name = "NEWS_ID", referencedColumnName = "NEWS_ID")
    @ManyToOne
    private News newsId;

    public NewsImageAssoc() {
    }

    public NewsImageAssoc(BigDecimal assocId) {
        this.assocId = assocId;
    }

    public BigDecimal getAssocId() {
        return assocId;
    }

    public void setAssocId(BigDecimal assocId) {
        this.assocId = assocId;
    }

    public NewsImage getImageId() {
        return imageId;
    }

    public void setImageId(NewsImage imageId) {
        this.imageId = imageId;
    }

    public News getNewsId() {
        return newsId;
    }

    public void setNewsId(News newsId) {
        this.newsId = newsId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (assocId != null ? assocId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NewsImageAssoc)) {
            return false;
        }
        NewsImageAssoc other = (NewsImageAssoc) object;
        if ((this.assocId == null && other.assocId != null) || (this.assocId != null && !this.assocId.equals(other.assocId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NewsPack.NewsImageAssoc[ assocId=" + assocId + " ]";
    }
    
}

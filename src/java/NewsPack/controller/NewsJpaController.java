/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import NewsPack.Views;
import NewsPack.NewsStatus;
import NewsPack.NewsCategory;
import NewsPack.Authors;
import NewsPack.Comments;
import NewsPack.News;
import java.util.ArrayList;
import java.util.Collection;
import NewsPack.NewsImageAssoc;
import NewsPack.controller.exceptions.NonexistentEntityException;
import NewsPack.controller.exceptions.PreexistingEntityException;
import NewsPack.controller.exceptions.RollbackFailureException;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author oracle
 */
public class NewsJpaController implements Serializable {

    public NewsJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(News news) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (news.getCommentsCollection() == null) {
            news.setCommentsCollection(new ArrayList<Comments>());
        }
        if (news.getNewsImageAssocCollection() == null) {
            news.setNewsImageAssocCollection(new ArrayList<NewsImageAssoc>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Views viewId = news.getViewId();
            if (viewId != null) {
                viewId = em.getReference(viewId.getClass(), viewId.getViewId());
                news.setViewId(viewId);
            }
            NewsStatus newsStatusId = news.getNewsStatusId();
            if (newsStatusId != null) {
                newsStatusId = em.getReference(newsStatusId.getClass(), newsStatusId.getNewsStatusId());
                news.setNewsStatusId(newsStatusId);
            }
            NewsCategory categoryId = news.getCategoryId();
            if (categoryId != null) {
                categoryId = em.getReference(categoryId.getClass(), categoryId.getCategoryId());
                news.setCategoryId(categoryId);
            }
            Authors authorId = news.getAuthorId();
            if (authorId != null) {
                authorId = em.getReference(authorId.getClass(), authorId.getAuthorId());
                news.setAuthorId(authorId);
            }
            Collection<Comments> attachedCommentsCollection = new ArrayList<Comments>();
            for (Comments commentsCollectionCommentsToAttach : news.getCommentsCollection()) {
                commentsCollectionCommentsToAttach = em.getReference(commentsCollectionCommentsToAttach.getClass(), commentsCollectionCommentsToAttach.getCommentId());
                attachedCommentsCollection.add(commentsCollectionCommentsToAttach);
            }
            news.setCommentsCollection(attachedCommentsCollection);
            Collection<NewsImageAssoc> attachedNewsImageAssocCollection = new ArrayList<NewsImageAssoc>();
            for (NewsImageAssoc newsImageAssocCollectionNewsImageAssocToAttach : news.getNewsImageAssocCollection()) {
                newsImageAssocCollectionNewsImageAssocToAttach = em.getReference(newsImageAssocCollectionNewsImageAssocToAttach.getClass(), newsImageAssocCollectionNewsImageAssocToAttach.getAssocId());
                attachedNewsImageAssocCollection.add(newsImageAssocCollectionNewsImageAssocToAttach);
            }
            news.setNewsImageAssocCollection(attachedNewsImageAssocCollection);
            em.persist(news);
            if (viewId != null) {
                viewId.getNewsCollection().add(news);
                viewId = em.merge(viewId);
            }
            if (newsStatusId != null) {
                newsStatusId.getNewsCollection().add(news);
                newsStatusId = em.merge(newsStatusId);
            }
            if (categoryId != null) {
                categoryId.getNewsCollection().add(news);
                categoryId = em.merge(categoryId);
            }
            if (authorId != null) {
                authorId.getNewsCollection().add(news);
                authorId = em.merge(authorId);
            }
            for (Comments commentsCollectionComments : news.getCommentsCollection()) {
                News oldNewsIdOfCommentsCollectionComments = commentsCollectionComments.getNewsId();
                commentsCollectionComments.setNewsId(news);
                commentsCollectionComments = em.merge(commentsCollectionComments);
                if (oldNewsIdOfCommentsCollectionComments != null) {
                    oldNewsIdOfCommentsCollectionComments.getCommentsCollection().remove(commentsCollectionComments);
                    oldNewsIdOfCommentsCollectionComments = em.merge(oldNewsIdOfCommentsCollectionComments);
                }
            }
            for (NewsImageAssoc newsImageAssocCollectionNewsImageAssoc : news.getNewsImageAssocCollection()) {
                News oldNewsIdOfNewsImageAssocCollectionNewsImageAssoc = newsImageAssocCollectionNewsImageAssoc.getNewsId();
                newsImageAssocCollectionNewsImageAssoc.setNewsId(news);
                newsImageAssocCollectionNewsImageAssoc = em.merge(newsImageAssocCollectionNewsImageAssoc);
                if (oldNewsIdOfNewsImageAssocCollectionNewsImageAssoc != null) {
                    oldNewsIdOfNewsImageAssocCollectionNewsImageAssoc.getNewsImageAssocCollection().remove(newsImageAssocCollectionNewsImageAssoc);
                    oldNewsIdOfNewsImageAssocCollectionNewsImageAssoc = em.merge(oldNewsIdOfNewsImageAssocCollectionNewsImageAssoc);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findNews(news.getNewsId()) != null) {
                throw new PreexistingEntityException("News " + news + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(News news) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            News persistentNews = em.find(News.class, news.getNewsId());
            Views viewIdOld = persistentNews.getViewId();
            Views viewIdNew = news.getViewId();
            NewsStatus newsStatusIdOld = persistentNews.getNewsStatusId();
            NewsStatus newsStatusIdNew = news.getNewsStatusId();
            NewsCategory categoryIdOld = persistentNews.getCategoryId();
            NewsCategory categoryIdNew = news.getCategoryId();
            Authors authorIdOld = persistentNews.getAuthorId();
            Authors authorIdNew = news.getAuthorId();
            Collection<Comments> commentsCollectionOld = persistentNews.getCommentsCollection();
            Collection<Comments> commentsCollectionNew = news.getCommentsCollection();
            Collection<NewsImageAssoc> newsImageAssocCollectionOld = persistentNews.getNewsImageAssocCollection();
            Collection<NewsImageAssoc> newsImageAssocCollectionNew = news.getNewsImageAssocCollection();
            if (viewIdNew != null) {
                viewIdNew = em.getReference(viewIdNew.getClass(), viewIdNew.getViewId());
                news.setViewId(viewIdNew);
            }
            if (newsStatusIdNew != null) {
                newsStatusIdNew = em.getReference(newsStatusIdNew.getClass(), newsStatusIdNew.getNewsStatusId());
                news.setNewsStatusId(newsStatusIdNew);
            }
            if (categoryIdNew != null) {
                categoryIdNew = em.getReference(categoryIdNew.getClass(), categoryIdNew.getCategoryId());
                news.setCategoryId(categoryIdNew);
            }
            if (authorIdNew != null) {
                authorIdNew = em.getReference(authorIdNew.getClass(), authorIdNew.getAuthorId());
                news.setAuthorId(authorIdNew);
            }
            Collection<Comments> attachedCommentsCollectionNew = new ArrayList<Comments>();
            for (Comments commentsCollectionNewCommentsToAttach : commentsCollectionNew) {
                commentsCollectionNewCommentsToAttach = em.getReference(commentsCollectionNewCommentsToAttach.getClass(), commentsCollectionNewCommentsToAttach.getCommentId());
                attachedCommentsCollectionNew.add(commentsCollectionNewCommentsToAttach);
            }
            commentsCollectionNew = attachedCommentsCollectionNew;
            news.setCommentsCollection(commentsCollectionNew);
            Collection<NewsImageAssoc> attachedNewsImageAssocCollectionNew = new ArrayList<NewsImageAssoc>();
            for (NewsImageAssoc newsImageAssocCollectionNewNewsImageAssocToAttach : newsImageAssocCollectionNew) {
                newsImageAssocCollectionNewNewsImageAssocToAttach = em.getReference(newsImageAssocCollectionNewNewsImageAssocToAttach.getClass(), newsImageAssocCollectionNewNewsImageAssocToAttach.getAssocId());
                attachedNewsImageAssocCollectionNew.add(newsImageAssocCollectionNewNewsImageAssocToAttach);
            }
            newsImageAssocCollectionNew = attachedNewsImageAssocCollectionNew;
            news.setNewsImageAssocCollection(newsImageAssocCollectionNew);
            news = em.merge(news);
            if (viewIdOld != null && !viewIdOld.equals(viewIdNew)) {
                viewIdOld.getNewsCollection().remove(news);
                viewIdOld = em.merge(viewIdOld);
            }
            if (viewIdNew != null && !viewIdNew.equals(viewIdOld)) {
                viewIdNew.getNewsCollection().add(news);
                viewIdNew = em.merge(viewIdNew);
            }
            if (newsStatusIdOld != null && !newsStatusIdOld.equals(newsStatusIdNew)) {
                newsStatusIdOld.getNewsCollection().remove(news);
                newsStatusIdOld = em.merge(newsStatusIdOld);
            }
            if (newsStatusIdNew != null && !newsStatusIdNew.equals(newsStatusIdOld)) {
                newsStatusIdNew.getNewsCollection().add(news);
                newsStatusIdNew = em.merge(newsStatusIdNew);
            }
            if (categoryIdOld != null && !categoryIdOld.equals(categoryIdNew)) {
                categoryIdOld.getNewsCollection().remove(news);
                categoryIdOld = em.merge(categoryIdOld);
            }
            if (categoryIdNew != null && !categoryIdNew.equals(categoryIdOld)) {
                categoryIdNew.getNewsCollection().add(news);
                categoryIdNew = em.merge(categoryIdNew);
            }
            if (authorIdOld != null && !authorIdOld.equals(authorIdNew)) {
                authorIdOld.getNewsCollection().remove(news);
                authorIdOld = em.merge(authorIdOld);
            }
            if (authorIdNew != null && !authorIdNew.equals(authorIdOld)) {
                authorIdNew.getNewsCollection().add(news);
                authorIdNew = em.merge(authorIdNew);
            }
            for (Comments commentsCollectionOldComments : commentsCollectionOld) {
                if (!commentsCollectionNew.contains(commentsCollectionOldComments)) {
                    commentsCollectionOldComments.setNewsId(null);
                    commentsCollectionOldComments = em.merge(commentsCollectionOldComments);
                }
            }
            for (Comments commentsCollectionNewComments : commentsCollectionNew) {
                if (!commentsCollectionOld.contains(commentsCollectionNewComments)) {
                    News oldNewsIdOfCommentsCollectionNewComments = commentsCollectionNewComments.getNewsId();
                    commentsCollectionNewComments.setNewsId(news);
                    commentsCollectionNewComments = em.merge(commentsCollectionNewComments);
                    if (oldNewsIdOfCommentsCollectionNewComments != null && !oldNewsIdOfCommentsCollectionNewComments.equals(news)) {
                        oldNewsIdOfCommentsCollectionNewComments.getCommentsCollection().remove(commentsCollectionNewComments);
                        oldNewsIdOfCommentsCollectionNewComments = em.merge(oldNewsIdOfCommentsCollectionNewComments);
                    }
                }
            }
            for (NewsImageAssoc newsImageAssocCollectionOldNewsImageAssoc : newsImageAssocCollectionOld) {
                if (!newsImageAssocCollectionNew.contains(newsImageAssocCollectionOldNewsImageAssoc)) {
                    newsImageAssocCollectionOldNewsImageAssoc.setNewsId(null);
                    newsImageAssocCollectionOldNewsImageAssoc = em.merge(newsImageAssocCollectionOldNewsImageAssoc);
                }
            }
            for (NewsImageAssoc newsImageAssocCollectionNewNewsImageAssoc : newsImageAssocCollectionNew) {
                if (!newsImageAssocCollectionOld.contains(newsImageAssocCollectionNewNewsImageAssoc)) {
                    News oldNewsIdOfNewsImageAssocCollectionNewNewsImageAssoc = newsImageAssocCollectionNewNewsImageAssoc.getNewsId();
                    newsImageAssocCollectionNewNewsImageAssoc.setNewsId(news);
                    newsImageAssocCollectionNewNewsImageAssoc = em.merge(newsImageAssocCollectionNewNewsImageAssoc);
                    if (oldNewsIdOfNewsImageAssocCollectionNewNewsImageAssoc != null && !oldNewsIdOfNewsImageAssocCollectionNewNewsImageAssoc.equals(news)) {
                        oldNewsIdOfNewsImageAssocCollectionNewNewsImageAssoc.getNewsImageAssocCollection().remove(newsImageAssocCollectionNewNewsImageAssoc);
                        oldNewsIdOfNewsImageAssocCollectionNewNewsImageAssoc = em.merge(oldNewsIdOfNewsImageAssocCollectionNewNewsImageAssoc);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigDecimal id = news.getNewsId();
                if (findNews(id) == null) {
                    throw new NonexistentEntityException("The news with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigDecimal id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            News news;
            try {
                news = em.getReference(News.class, id);
                news.getNewsId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The news with id " + id + " no longer exists.", enfe);
            }
            Views viewId = news.getViewId();
            if (viewId != null) {
                viewId.getNewsCollection().remove(news);
                viewId = em.merge(viewId);
            }
            NewsStatus newsStatusId = news.getNewsStatusId();
            if (newsStatusId != null) {
                newsStatusId.getNewsCollection().remove(news);
                newsStatusId = em.merge(newsStatusId);
            }
            NewsCategory categoryId = news.getCategoryId();
            if (categoryId != null) {
                categoryId.getNewsCollection().remove(news);
                categoryId = em.merge(categoryId);
            }
            Authors authorId = news.getAuthorId();
            if (authorId != null) {
                authorId.getNewsCollection().remove(news);
                authorId = em.merge(authorId);
            }
            Collection<Comments> commentsCollection = news.getCommentsCollection();
            for (Comments commentsCollectionComments : commentsCollection) {
                commentsCollectionComments.setNewsId(null);
                commentsCollectionComments = em.merge(commentsCollectionComments);
            }
            Collection<NewsImageAssoc> newsImageAssocCollection = news.getNewsImageAssocCollection();
            for (NewsImageAssoc newsImageAssocCollectionNewsImageAssoc : newsImageAssocCollection) {
                newsImageAssocCollectionNewsImageAssoc.setNewsId(null);
                newsImageAssocCollectionNewsImageAssoc = em.merge(newsImageAssocCollectionNewsImageAssoc);
            }
            em.remove(news);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<News> findNewsEntities() {
        return findNewsEntities(true, -1, -1);
    }

    public List<News> findNewsEntities(int maxResults, int firstResult) {
        return findNewsEntities(false, maxResults, firstResult);
    }

    private List<News> findNewsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from News as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public News findNews(BigDecimal id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(News.class, id);
        } finally {
            em.close();
        }
    }

    public int getNewsCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from News as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

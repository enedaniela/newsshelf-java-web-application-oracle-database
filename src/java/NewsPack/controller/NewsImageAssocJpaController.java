/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import NewsPack.NewsImage;
import NewsPack.News;
import NewsPack.NewsImageAssoc;
import NewsPack.controller.exceptions.NonexistentEntityException;
import NewsPack.controller.exceptions.PreexistingEntityException;
import NewsPack.controller.exceptions.RollbackFailureException;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author oracle
 */
public class NewsImageAssocJpaController implements Serializable {

    public NewsImageAssocJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(NewsImageAssoc newsImageAssoc) throws PreexistingEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            NewsImage imageId = newsImageAssoc.getImageId();
            if (imageId != null) {
                imageId = em.getReference(imageId.getClass(), imageId.getImageId());
                newsImageAssoc.setImageId(imageId);
            }
            News newsId = newsImageAssoc.getNewsId();
            if (newsId != null) {
                newsId = em.getReference(newsId.getClass(), newsId.getNewsId());
                newsImageAssoc.setNewsId(newsId);
            }
            em.persist(newsImageAssoc);
            if (imageId != null) {
                imageId.getNewsImageAssocCollection().add(newsImageAssoc);
                imageId = em.merge(imageId);
            }
            if (newsId != null) {
                newsId.getNewsImageAssocCollection().add(newsImageAssoc);
                newsId = em.merge(newsId);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findNewsImageAssoc(newsImageAssoc.getAssocId()) != null) {
                throw new PreexistingEntityException("NewsImageAssoc " + newsImageAssoc + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(NewsImageAssoc newsImageAssoc) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            NewsImageAssoc persistentNewsImageAssoc = em.find(NewsImageAssoc.class, newsImageAssoc.getAssocId());
            NewsImage imageIdOld = persistentNewsImageAssoc.getImageId();
            NewsImage imageIdNew = newsImageAssoc.getImageId();
            News newsIdOld = persistentNewsImageAssoc.getNewsId();
            News newsIdNew = newsImageAssoc.getNewsId();
            if (imageIdNew != null) {
                imageIdNew = em.getReference(imageIdNew.getClass(), imageIdNew.getImageId());
                newsImageAssoc.setImageId(imageIdNew);
            }
            if (newsIdNew != null) {
                newsIdNew = em.getReference(newsIdNew.getClass(), newsIdNew.getNewsId());
                newsImageAssoc.setNewsId(newsIdNew);
            }
            newsImageAssoc = em.merge(newsImageAssoc);
            if (imageIdOld != null && !imageIdOld.equals(imageIdNew)) {
                imageIdOld.getNewsImageAssocCollection().remove(newsImageAssoc);
                imageIdOld = em.merge(imageIdOld);
            }
            if (imageIdNew != null && !imageIdNew.equals(imageIdOld)) {
                imageIdNew.getNewsImageAssocCollection().add(newsImageAssoc);
                imageIdNew = em.merge(imageIdNew);
            }
            if (newsIdOld != null && !newsIdOld.equals(newsIdNew)) {
                newsIdOld.getNewsImageAssocCollection().remove(newsImageAssoc);
                newsIdOld = em.merge(newsIdOld);
            }
            if (newsIdNew != null && !newsIdNew.equals(newsIdOld)) {
                newsIdNew.getNewsImageAssocCollection().add(newsImageAssoc);
                newsIdNew = em.merge(newsIdNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigDecimal id = newsImageAssoc.getAssocId();
                if (findNewsImageAssoc(id) == null) {
                    throw new NonexistentEntityException("The newsImageAssoc with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigDecimal id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            NewsImageAssoc newsImageAssoc;
            try {
                newsImageAssoc = em.getReference(NewsImageAssoc.class, id);
                newsImageAssoc.getAssocId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The newsImageAssoc with id " + id + " no longer exists.", enfe);
            }
            NewsImage imageId = newsImageAssoc.getImageId();
            if (imageId != null) {
                imageId.getNewsImageAssocCollection().remove(newsImageAssoc);
                imageId = em.merge(imageId);
            }
            News newsId = newsImageAssoc.getNewsId();
            if (newsId != null) {
                newsId.getNewsImageAssocCollection().remove(newsImageAssoc);
                newsId = em.merge(newsId);
            }
            em.remove(newsImageAssoc);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NewsImageAssoc> findNewsImageAssocEntities() {
        return findNewsImageAssocEntities(true, -1, -1);
    }

    public List<NewsImageAssoc> findNewsImageAssocEntities(int maxResults, int firstResult) {
        return findNewsImageAssocEntities(false, maxResults, firstResult);
    }

    private List<NewsImageAssoc> findNewsImageAssocEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from NewsImageAssoc as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public NewsImageAssoc findNewsImageAssoc(BigDecimal id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NewsImageAssoc.class, id);
        } finally {
            em.close();
        }
    }

    public int getNewsImageAssocCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from NewsImageAssoc as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

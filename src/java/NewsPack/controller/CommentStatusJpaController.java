/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack.controller;

import NewsPack.CommentStatus;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import NewsPack.Comments;
import NewsPack.controller.exceptions.NonexistentEntityException;
import NewsPack.controller.exceptions.PreexistingEntityException;
import NewsPack.controller.exceptions.RollbackFailureException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author oracle
 */
public class CommentStatusJpaController implements Serializable {

    public CommentStatusJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CommentStatus commentStatus) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (commentStatus.getCommentsCollection() == null) {
            commentStatus.setCommentsCollection(new ArrayList<Comments>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<Comments> attachedCommentsCollection = new ArrayList<Comments>();
            for (Comments commentsCollectionCommentsToAttach : commentStatus.getCommentsCollection()) {
                commentsCollectionCommentsToAttach = em.getReference(commentsCollectionCommentsToAttach.getClass(), commentsCollectionCommentsToAttach.getCommentId());
                attachedCommentsCollection.add(commentsCollectionCommentsToAttach);
            }
            commentStatus.setCommentsCollection(attachedCommentsCollection);
            em.persist(commentStatus);
            for (Comments commentsCollectionComments : commentStatus.getCommentsCollection()) {
                CommentStatus oldCommentStatusIdOfCommentsCollectionComments = commentsCollectionComments.getCommentStatusId();
                commentsCollectionComments.setCommentStatusId(commentStatus);
                commentsCollectionComments = em.merge(commentsCollectionComments);
                if (oldCommentStatusIdOfCommentsCollectionComments != null) {
                    oldCommentStatusIdOfCommentsCollectionComments.getCommentsCollection().remove(commentsCollectionComments);
                    oldCommentStatusIdOfCommentsCollectionComments = em.merge(oldCommentStatusIdOfCommentsCollectionComments);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findCommentStatus(commentStatus.getCommentStatusId()) != null) {
                throw new PreexistingEntityException("CommentStatus " + commentStatus + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CommentStatus commentStatus) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            CommentStatus persistentCommentStatus = em.find(CommentStatus.class, commentStatus.getCommentStatusId());
            Collection<Comments> commentsCollectionOld = persistentCommentStatus.getCommentsCollection();
            Collection<Comments> commentsCollectionNew = commentStatus.getCommentsCollection();
            Collection<Comments> attachedCommentsCollectionNew = new ArrayList<Comments>();
            for (Comments commentsCollectionNewCommentsToAttach : commentsCollectionNew) {
                commentsCollectionNewCommentsToAttach = em.getReference(commentsCollectionNewCommentsToAttach.getClass(), commentsCollectionNewCommentsToAttach.getCommentId());
                attachedCommentsCollectionNew.add(commentsCollectionNewCommentsToAttach);
            }
            commentsCollectionNew = attachedCommentsCollectionNew;
            commentStatus.setCommentsCollection(commentsCollectionNew);
            commentStatus = em.merge(commentStatus);
            for (Comments commentsCollectionOldComments : commentsCollectionOld) {
                if (!commentsCollectionNew.contains(commentsCollectionOldComments)) {
                    commentsCollectionOldComments.setCommentStatusId(null);
                    commentsCollectionOldComments = em.merge(commentsCollectionOldComments);
                }
            }
            for (Comments commentsCollectionNewComments : commentsCollectionNew) {
                if (!commentsCollectionOld.contains(commentsCollectionNewComments)) {
                    CommentStatus oldCommentStatusIdOfCommentsCollectionNewComments = commentsCollectionNewComments.getCommentStatusId();
                    commentsCollectionNewComments.setCommentStatusId(commentStatus);
                    commentsCollectionNewComments = em.merge(commentsCollectionNewComments);
                    if (oldCommentStatusIdOfCommentsCollectionNewComments != null && !oldCommentStatusIdOfCommentsCollectionNewComments.equals(commentStatus)) {
                        oldCommentStatusIdOfCommentsCollectionNewComments.getCommentsCollection().remove(commentsCollectionNewComments);
                        oldCommentStatusIdOfCommentsCollectionNewComments = em.merge(oldCommentStatusIdOfCommentsCollectionNewComments);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigDecimal id = commentStatus.getCommentStatusId();
                if (findCommentStatus(id) == null) {
                    throw new NonexistentEntityException("The commentStatus with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigDecimal id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            CommentStatus commentStatus;
            try {
                commentStatus = em.getReference(CommentStatus.class, id);
                commentStatus.getCommentStatusId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The commentStatus with id " + id + " no longer exists.", enfe);
            }
            Collection<Comments> commentsCollection = commentStatus.getCommentsCollection();
            for (Comments commentsCollectionComments : commentsCollection) {
                commentsCollectionComments.setCommentStatusId(null);
                commentsCollectionComments = em.merge(commentsCollectionComments);
            }
            em.remove(commentStatus);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CommentStatus> findCommentStatusEntities() {
        return findCommentStatusEntities(true, -1, -1);
    }

    public List<CommentStatus> findCommentStatusEntities(int maxResults, int firstResult) {
        return findCommentStatusEntities(false, maxResults, firstResult);
    }

    private List<CommentStatus> findCommentStatusEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from CommentStatus as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CommentStatus findCommentStatus(BigDecimal id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CommentStatus.class, id);
        } finally {
            em.close();
        }
    }

    public int getCommentStatusCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from CommentStatus as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

      $( document ).ready(function() {

    //functie GET care aduce inregistrari din baza de date
    $.get( "webresources/newspack.news", function( data ) {

        var x = data.getElementsByTagName("news");
        var html="";
        for (i = 0; i <x.length; i++) {
            //parsare xml si construire html dinamic
            var currentDate = new Date(data.getElementsByTagName("publicDate")[i].childNodes[0].nodeValue);

            //construiesc preview-ul
            var str = data.getElementsByTagName("content")[i].childNodes[0].nodeValue;
            var res = str.substring(0,200);

            //extrag id-ul de stire pentru a-l trimite catre ecranul entitate
            var newsId = data.getElementsByTagName("newsId")[i].childNodes[0].nodeValue;

            html = html + 
                   '<div class="entire-news">'+
                       '<div class="news">\n\
                           <img src="'+data.getElementsByTagName("titleImage")[i].childNodes[0].nodeValue  +'" alt="Smiley face" height="300px" width="80%">'+
                           '<div class="title"> <a href="news_entity.jsp?newsId='+newsId+'">'+data.getElementsByTagName("title")[i].childNodes[0].nodeValue +'</a></div>'+
                           '<p class="preview">' + res +'...<p> \n\
                       </div>\n\
                       <div class="info">\n\
                           <div class="author">'+data.getElementsByTagName("authorId")[2*i].childNodes[3].textContent+' '+data.getElementsByTagName("authorId")[2*i].childNodes[5].textContent+'</div>\n\
                           <div class="publ-date">'+currentDate.toDateString()+'</div>\n\
                       </div>\n\
                       <div class="media"> \n\
                           <div class="facebook"> <img src="images/facebook.jpg"> </div> \n\
                           <div class="tweeter"><img src="images/tw.png"> </div> \n\
                       </div> \n\
                   </div>';
        }
        //afisare html dinamic in pagina
        document.getElementById("content").innerHTML = html;     
  });
});
//asa inserezi o inregistrare in baza de date
function submit_auth_status(){
    //iei informatiile de care ai nevoie
    var stat = document.getElementById("status").value;
    var today = new Date();

    //construiesti un xml ca ala din Test RESTful Application
    var xml = '<authorStatus> <authorStatusId>31</authorStatusId><createDate>2018-02-25T00:00:00+02:00</createDate><description>'+stat+'</description></authorStatus> ';
    console.log(xml);

    //faci un call de ajax
    $.ajax("webresources/newspack.authorstatus", {
	headers: { 
        'Accept': 'application/xml',
        'Content-Type': 'application/xml' 
    },
	data:xml,
	contenType: "application/xml; charset=UTF-8",
	dataType : 'xml',
	method: 'POST'
});
}
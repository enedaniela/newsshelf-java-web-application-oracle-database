/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author oracle
 */
@Entity
@Table(name = "AUTHOR_STATUS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AuthorStatus.findAll", query = "SELECT a FROM AuthorStatus a"),
    @NamedQuery(name = "AuthorStatus.findByAuthorStatusId", query = "SELECT a FROM AuthorStatus a WHERE a.authorStatusId = :authorStatusId"),
    @NamedQuery(name = "AuthorStatus.findByDescription", query = "SELECT a FROM AuthorStatus a WHERE a.description = :description"),
    @NamedQuery(name = "AuthorStatus.findByCreateDate", query = "SELECT a FROM AuthorStatus a WHERE a.createDate = :createDate")})
public class AuthorStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AUTHOR_STATUS_ID")
    private BigDecimal authorStatusId;
    @Size(max = 100)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @OneToMany(mappedBy = "authorStatusId")
    private Collection<Authors> authorsCollection;

    public AuthorStatus() {
    }

    public AuthorStatus(BigDecimal authorStatusId) {
        this.authorStatusId = authorStatusId;
    }

    public BigDecimal getAuthorStatusId() {
        return authorStatusId;
    }

    public void setAuthorStatusId(BigDecimal authorStatusId) {
        this.authorStatusId = authorStatusId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @XmlTransient
    public Collection<Authors> getAuthorsCollection() {
        return authorsCollection;
    }

    public void setAuthorsCollection(Collection<Authors> authorsCollection) {
        this.authorsCollection = authorsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (authorStatusId != null ? authorStatusId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AuthorStatus)) {
            return false;
        }
        AuthorStatus other = (AuthorStatus) object;
        if ((this.authorStatusId == null && other.authorStatusId != null) || (this.authorStatusId != null && !this.authorStatusId.equals(other.authorStatusId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NewsPack.AuthorStatus[ authorStatusId=" + authorStatusId + " ]";
    }
    
}

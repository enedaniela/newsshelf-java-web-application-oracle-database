<%-- 
    Document   : news_entity
    Created on : Apr 11, 2018, 3:16:04 PM
    Author     : oracle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>
    <body>
        <h1>See console for newsId!</h1>
    </body>
    
    <script>
        
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }
    
    $( document ).ready(function() {
        var param = getUrlVars();
        console.log(param["newsId"]);
    });
    
    </script>
</html>

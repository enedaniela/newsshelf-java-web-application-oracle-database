/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import NewsPack.AuthorStatus;
import NewsPack.Authors;
import NewsPack.News;
import NewsPack.controller.exceptions.IllegalOrphanException;
import NewsPack.controller.exceptions.NonexistentEntityException;
import NewsPack.controller.exceptions.PreexistingEntityException;
import NewsPack.controller.exceptions.RollbackFailureException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author oracle
 */
public class AuthorsJpaController implements Serializable {

    public AuthorsJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Authors authors) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (authors.getNewsCollection() == null) {
            authors.setNewsCollection(new ArrayList<News>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            AuthorStatus authorStatusId = authors.getAuthorStatusId();
            if (authorStatusId != null) {
                authorStatusId = em.getReference(authorStatusId.getClass(), authorStatusId.getAuthorStatusId());
                authors.setAuthorStatusId(authorStatusId);
            }
            Collection<News> attachedNewsCollection = new ArrayList<News>();
            for (News newsCollectionNewsToAttach : authors.getNewsCollection()) {
                newsCollectionNewsToAttach = em.getReference(newsCollectionNewsToAttach.getClass(), newsCollectionNewsToAttach.getNewsId());
                attachedNewsCollection.add(newsCollectionNewsToAttach);
            }
            authors.setNewsCollection(attachedNewsCollection);
            em.persist(authors);
            if (authorStatusId != null) {
                authorStatusId.getAuthorsCollection().add(authors);
                authorStatusId = em.merge(authorStatusId);
            }
            for (News newsCollectionNews : authors.getNewsCollection()) {
                Authors oldAuthorIdOfNewsCollectionNews = newsCollectionNews.getAuthorId();
                newsCollectionNews.setAuthorId(authors);
                newsCollectionNews = em.merge(newsCollectionNews);
                if (oldAuthorIdOfNewsCollectionNews != null) {
                    oldAuthorIdOfNewsCollectionNews.getNewsCollection().remove(newsCollectionNews);
                    oldAuthorIdOfNewsCollectionNews = em.merge(oldAuthorIdOfNewsCollectionNews);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findAuthors(authors.getAuthorId()) != null) {
                throw new PreexistingEntityException("Authors " + authors + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Authors authors) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Authors persistentAuthors = em.find(Authors.class, authors.getAuthorId());
            AuthorStatus authorStatusIdOld = persistentAuthors.getAuthorStatusId();
            AuthorStatus authorStatusIdNew = authors.getAuthorStatusId();
            Collection<News> newsCollectionOld = persistentAuthors.getNewsCollection();
            Collection<News> newsCollectionNew = authors.getNewsCollection();
            List<String> illegalOrphanMessages = null;
            for (News newsCollectionOldNews : newsCollectionOld) {
                if (!newsCollectionNew.contains(newsCollectionOldNews)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain News " + newsCollectionOldNews + " since its authorId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (authorStatusIdNew != null) {
                authorStatusIdNew = em.getReference(authorStatusIdNew.getClass(), authorStatusIdNew.getAuthorStatusId());
                authors.setAuthorStatusId(authorStatusIdNew);
            }
            Collection<News> attachedNewsCollectionNew = new ArrayList<News>();
            for (News newsCollectionNewNewsToAttach : newsCollectionNew) {
                newsCollectionNewNewsToAttach = em.getReference(newsCollectionNewNewsToAttach.getClass(), newsCollectionNewNewsToAttach.getNewsId());
                attachedNewsCollectionNew.add(newsCollectionNewNewsToAttach);
            }
            newsCollectionNew = attachedNewsCollectionNew;
            authors.setNewsCollection(newsCollectionNew);
            authors = em.merge(authors);
            if (authorStatusIdOld != null && !authorStatusIdOld.equals(authorStatusIdNew)) {
                authorStatusIdOld.getAuthorsCollection().remove(authors);
                authorStatusIdOld = em.merge(authorStatusIdOld);
            }
            if (authorStatusIdNew != null && !authorStatusIdNew.equals(authorStatusIdOld)) {
                authorStatusIdNew.getAuthorsCollection().add(authors);
                authorStatusIdNew = em.merge(authorStatusIdNew);
            }
            for (News newsCollectionNewNews : newsCollectionNew) {
                if (!newsCollectionOld.contains(newsCollectionNewNews)) {
                    Authors oldAuthorIdOfNewsCollectionNewNews = newsCollectionNewNews.getAuthorId();
                    newsCollectionNewNews.setAuthorId(authors);
                    newsCollectionNewNews = em.merge(newsCollectionNewNews);
                    if (oldAuthorIdOfNewsCollectionNewNews != null && !oldAuthorIdOfNewsCollectionNewNews.equals(authors)) {
                        oldAuthorIdOfNewsCollectionNewNews.getNewsCollection().remove(newsCollectionNewNews);
                        oldAuthorIdOfNewsCollectionNewNews = em.merge(oldAuthorIdOfNewsCollectionNewNews);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigDecimal id = authors.getAuthorId();
                if (findAuthors(id) == null) {
                    throw new NonexistentEntityException("The authors with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigDecimal id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Authors authors;
            try {
                authors = em.getReference(Authors.class, id);
                authors.getAuthorId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The authors with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<News> newsCollectionOrphanCheck = authors.getNewsCollection();
            for (News newsCollectionOrphanCheckNews : newsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Authors (" + authors + ") cannot be destroyed since the News " + newsCollectionOrphanCheckNews + " in its newsCollection field has a non-nullable authorId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            AuthorStatus authorStatusId = authors.getAuthorStatusId();
            if (authorStatusId != null) {
                authorStatusId.getAuthorsCollection().remove(authors);
                authorStatusId = em.merge(authorStatusId);
            }
            em.remove(authors);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Authors> findAuthorsEntities() {
        return findAuthorsEntities(true, -1, -1);
    }

    public List<Authors> findAuthorsEntities(int maxResults, int firstResult) {
        return findAuthorsEntities(false, maxResults, firstResult);
    }

    private List<Authors> findAuthorsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Authors as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Authors findAuthors(BigDecimal id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Authors.class, id);
        } finally {
            em.close();
        }
    }

    public int getAuthorsCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Authors as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

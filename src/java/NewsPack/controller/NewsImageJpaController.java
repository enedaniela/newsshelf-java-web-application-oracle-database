/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack.controller;

import NewsPack.NewsImage;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import NewsPack.NewsImageAssoc;
import NewsPack.controller.exceptions.NonexistentEntityException;
import NewsPack.controller.exceptions.PreexistingEntityException;
import NewsPack.controller.exceptions.RollbackFailureException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author oracle
 */
public class NewsImageJpaController implements Serializable {

    public NewsImageJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(NewsImage newsImage) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (newsImage.getNewsImageAssocCollection() == null) {
            newsImage.setNewsImageAssocCollection(new ArrayList<NewsImageAssoc>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<NewsImageAssoc> attachedNewsImageAssocCollection = new ArrayList<NewsImageAssoc>();
            for (NewsImageAssoc newsImageAssocCollectionNewsImageAssocToAttach : newsImage.getNewsImageAssocCollection()) {
                newsImageAssocCollectionNewsImageAssocToAttach = em.getReference(newsImageAssocCollectionNewsImageAssocToAttach.getClass(), newsImageAssocCollectionNewsImageAssocToAttach.getAssocId());
                attachedNewsImageAssocCollection.add(newsImageAssocCollectionNewsImageAssocToAttach);
            }
            newsImage.setNewsImageAssocCollection(attachedNewsImageAssocCollection);
            em.persist(newsImage);
            for (NewsImageAssoc newsImageAssocCollectionNewsImageAssoc : newsImage.getNewsImageAssocCollection()) {
                NewsImage oldImageIdOfNewsImageAssocCollectionNewsImageAssoc = newsImageAssocCollectionNewsImageAssoc.getImageId();
                newsImageAssocCollectionNewsImageAssoc.setImageId(newsImage);
                newsImageAssocCollectionNewsImageAssoc = em.merge(newsImageAssocCollectionNewsImageAssoc);
                if (oldImageIdOfNewsImageAssocCollectionNewsImageAssoc != null) {
                    oldImageIdOfNewsImageAssocCollectionNewsImageAssoc.getNewsImageAssocCollection().remove(newsImageAssocCollectionNewsImageAssoc);
                    oldImageIdOfNewsImageAssocCollectionNewsImageAssoc = em.merge(oldImageIdOfNewsImageAssocCollectionNewsImageAssoc);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findNewsImage(newsImage.getImageId()) != null) {
                throw new PreexistingEntityException("NewsImage " + newsImage + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(NewsImage newsImage) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            NewsImage persistentNewsImage = em.find(NewsImage.class, newsImage.getImageId());
            Collection<NewsImageAssoc> newsImageAssocCollectionOld = persistentNewsImage.getNewsImageAssocCollection();
            Collection<NewsImageAssoc> newsImageAssocCollectionNew = newsImage.getNewsImageAssocCollection();
            Collection<NewsImageAssoc> attachedNewsImageAssocCollectionNew = new ArrayList<NewsImageAssoc>();
            for (NewsImageAssoc newsImageAssocCollectionNewNewsImageAssocToAttach : newsImageAssocCollectionNew) {
                newsImageAssocCollectionNewNewsImageAssocToAttach = em.getReference(newsImageAssocCollectionNewNewsImageAssocToAttach.getClass(), newsImageAssocCollectionNewNewsImageAssocToAttach.getAssocId());
                attachedNewsImageAssocCollectionNew.add(newsImageAssocCollectionNewNewsImageAssocToAttach);
            }
            newsImageAssocCollectionNew = attachedNewsImageAssocCollectionNew;
            newsImage.setNewsImageAssocCollection(newsImageAssocCollectionNew);
            newsImage = em.merge(newsImage);
            for (NewsImageAssoc newsImageAssocCollectionOldNewsImageAssoc : newsImageAssocCollectionOld) {
                if (!newsImageAssocCollectionNew.contains(newsImageAssocCollectionOldNewsImageAssoc)) {
                    newsImageAssocCollectionOldNewsImageAssoc.setImageId(null);
                    newsImageAssocCollectionOldNewsImageAssoc = em.merge(newsImageAssocCollectionOldNewsImageAssoc);
                }
            }
            for (NewsImageAssoc newsImageAssocCollectionNewNewsImageAssoc : newsImageAssocCollectionNew) {
                if (!newsImageAssocCollectionOld.contains(newsImageAssocCollectionNewNewsImageAssoc)) {
                    NewsImage oldImageIdOfNewsImageAssocCollectionNewNewsImageAssoc = newsImageAssocCollectionNewNewsImageAssoc.getImageId();
                    newsImageAssocCollectionNewNewsImageAssoc.setImageId(newsImage);
                    newsImageAssocCollectionNewNewsImageAssoc = em.merge(newsImageAssocCollectionNewNewsImageAssoc);
                    if (oldImageIdOfNewsImageAssocCollectionNewNewsImageAssoc != null && !oldImageIdOfNewsImageAssocCollectionNewNewsImageAssoc.equals(newsImage)) {
                        oldImageIdOfNewsImageAssocCollectionNewNewsImageAssoc.getNewsImageAssocCollection().remove(newsImageAssocCollectionNewNewsImageAssoc);
                        oldImageIdOfNewsImageAssocCollectionNewNewsImageAssoc = em.merge(oldImageIdOfNewsImageAssocCollectionNewNewsImageAssoc);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigDecimal id = newsImage.getImageId();
                if (findNewsImage(id) == null) {
                    throw new NonexistentEntityException("The newsImage with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigDecimal id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            NewsImage newsImage;
            try {
                newsImage = em.getReference(NewsImage.class, id);
                newsImage.getImageId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The newsImage with id " + id + " no longer exists.", enfe);
            }
            Collection<NewsImageAssoc> newsImageAssocCollection = newsImage.getNewsImageAssocCollection();
            for (NewsImageAssoc newsImageAssocCollectionNewsImageAssoc : newsImageAssocCollection) {
                newsImageAssocCollectionNewsImageAssoc.setImageId(null);
                newsImageAssocCollectionNewsImageAssoc = em.merge(newsImageAssocCollectionNewsImageAssoc);
            }
            em.remove(newsImage);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NewsImage> findNewsImageEntities() {
        return findNewsImageEntities(true, -1, -1);
    }

    public List<NewsImage> findNewsImageEntities(int maxResults, int firstResult) {
        return findNewsImageEntities(false, maxResults, firstResult);
    }

    private List<NewsImage> findNewsImageEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from NewsImage as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public NewsImage findNewsImage(BigDecimal id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NewsImage.class, id);
        } finally {
            em.close();
        }
    }

    public int getNewsImageCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from NewsImage as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

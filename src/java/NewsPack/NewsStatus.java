/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author oracle
 */
@Entity
@Table(name = "NEWS_STATUS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NewsStatus.findAll", query = "SELECT n FROM NewsStatus n"),
    @NamedQuery(name = "NewsStatus.findByNewsStatusId", query = "SELECT n FROM NewsStatus n WHERE n.newsStatusId = :newsStatusId"),
    @NamedQuery(name = "NewsStatus.findByDescription", query = "SELECT n FROM NewsStatus n WHERE n.description = :description"),
    @NamedQuery(name = "NewsStatus.findByCreateDate", query = "SELECT n FROM NewsStatus n WHERE n.createDate = :createDate")})
public class NewsStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "NEWS_STATUS_ID")
    private BigDecimal newsStatusId;
    @Size(max = 100)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "newsStatusId")
    private Collection<News> newsCollection;

    public NewsStatus() {
    }

    public NewsStatus(BigDecimal newsStatusId) {
        this.newsStatusId = newsStatusId;
    }

    public BigDecimal getNewsStatusId() {
        return newsStatusId;
    }

    public void setNewsStatusId(BigDecimal newsStatusId) {
        this.newsStatusId = newsStatusId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @XmlTransient
    public Collection<News> getNewsCollection() {
        return newsCollection;
    }

    public void setNewsCollection(Collection<News> newsCollection) {
        this.newsCollection = newsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (newsStatusId != null ? newsStatusId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NewsStatus)) {
            return false;
        }
        NewsStatus other = (NewsStatus) object;
        if ((this.newsStatusId == null && other.newsStatusId != null) || (this.newsStatusId != null && !this.newsStatusId.equals(other.newsStatusId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NewsPack.NewsStatus[ newsStatusId=" + newsStatusId + " ]";
    }
    
}

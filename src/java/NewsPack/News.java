/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author oracle
 */
@Entity
@Table(name = "NEWS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "News.findAll", query = "SELECT n FROM News n"),
    @NamedQuery(name = "News.findByNewsId", query = "SELECT n FROM News n WHERE n.newsId = :newsId"),
    @NamedQuery(name = "News.findByTitle", query = "SELECT n FROM News n WHERE n.title = :title"),
    @NamedQuery(name = "News.findByPublicDate", query = "SELECT n FROM News n WHERE n.publicDate = :publicDate"),
    @NamedQuery(name = "News.findByContent", query = "SELECT n FROM News n WHERE n.content = :content"),
    @NamedQuery(name = "News.findByTitleImage", query = "SELECT n FROM News n WHERE n.titleImage = :titleImage")})
public class News implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "NEWS_ID")
    private BigDecimal newsId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "TITLE")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PUBLIC_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date publicDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4000)
    @Column(name = "CONTENT")
    private String content;
    @Size(max = 500)
    @Column(name = "TITLE_IMAGE")
    private String titleImage;
    @JoinColumn(name = "VIEW_ID", referencedColumnName = "VIEW_ID")
    @ManyToOne
    private Views viewId;
    @JoinColumn(name = "NEWS_STATUS_ID", referencedColumnName = "NEWS_STATUS_ID")
    @ManyToOne(optional = false)
    private NewsStatus newsStatusId;
    @JoinColumn(name = "CATEGORY_ID", referencedColumnName = "CATEGORY_ID")
    @ManyToOne(optional = false)
    private NewsCategory categoryId;
    @JoinColumn(name = "AUTHOR_ID", referencedColumnName = "AUTHOR_ID")
    @ManyToOne(optional = false)
    private Authors authorId;
    @OneToMany(mappedBy = "newsId")
    private Collection<Comments> commentsCollection;
    @OneToMany(mappedBy = "newsId")
    private Collection<NewsImageAssoc> newsImageAssocCollection;

    public News() {
    }

    public News(BigDecimal newsId) {
        this.newsId = newsId;
    }

    public News(BigDecimal newsId, String title, Date publicDate, String content) {
        this.newsId = newsId;
        this.title = title;
        this.publicDate = publicDate;
        this.content = content;
    }

    public BigDecimal getNewsId() {
        return newsId;
    }

    public void setNewsId(BigDecimal newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPublicDate() {
        return publicDate;
    }

    public void setPublicDate(Date publicDate) {
        this.publicDate = publicDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitleImage() {
        return titleImage;
    }

    public void setTitleImage(String titleImage) {
        this.titleImage = titleImage;
    }

    public Views getViewId() {
        return viewId;
    }

    public void setViewId(Views viewId) {
        this.viewId = viewId;
    }

    public NewsStatus getNewsStatusId() {
        return newsStatusId;
    }

    public void setNewsStatusId(NewsStatus newsStatusId) {
        this.newsStatusId = newsStatusId;
    }

    public NewsCategory getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(NewsCategory categoryId) {
        this.categoryId = categoryId;
    }

    public Authors getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Authors authorId) {
        this.authorId = authorId;
    }

    @XmlTransient
    public Collection<Comments> getCommentsCollection() {
        return commentsCollection;
    }

    public void setCommentsCollection(Collection<Comments> commentsCollection) {
        this.commentsCollection = commentsCollection;
    }

    @XmlTransient
    public Collection<NewsImageAssoc> getNewsImageAssocCollection() {
        return newsImageAssocCollection;
    }

    public void setNewsImageAssocCollection(Collection<NewsImageAssoc> newsImageAssocCollection) {
        this.newsImageAssocCollection = newsImageAssocCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (newsId != null ? newsId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof News)) {
            return false;
        }
        News other = (News) object;
        if ((this.newsId == null && other.newsId != null) || (this.newsId != null && !this.newsId.equals(other.newsId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NewsPack.News[ newsId=" + newsId + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack.controller;

import NewsPack.CategoryStatus;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import NewsPack.NewsCategory;
import NewsPack.controller.exceptions.NonexistentEntityException;
import NewsPack.controller.exceptions.PreexistingEntityException;
import NewsPack.controller.exceptions.RollbackFailureException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author oracle
 */
public class CategoryStatusJpaController implements Serializable {

    public CategoryStatusJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CategoryStatus categoryStatus) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (categoryStatus.getNewsCategoryCollection() == null) {
            categoryStatus.setNewsCategoryCollection(new ArrayList<NewsCategory>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<NewsCategory> attachedNewsCategoryCollection = new ArrayList<NewsCategory>();
            for (NewsCategory newsCategoryCollectionNewsCategoryToAttach : categoryStatus.getNewsCategoryCollection()) {
                newsCategoryCollectionNewsCategoryToAttach = em.getReference(newsCategoryCollectionNewsCategoryToAttach.getClass(), newsCategoryCollectionNewsCategoryToAttach.getCategoryId());
                attachedNewsCategoryCollection.add(newsCategoryCollectionNewsCategoryToAttach);
            }
            categoryStatus.setNewsCategoryCollection(attachedNewsCategoryCollection);
            em.persist(categoryStatus);
            for (NewsCategory newsCategoryCollectionNewsCategory : categoryStatus.getNewsCategoryCollection()) {
                CategoryStatus oldCategoryStatusIdOfNewsCategoryCollectionNewsCategory = newsCategoryCollectionNewsCategory.getCategoryStatusId();
                newsCategoryCollectionNewsCategory.setCategoryStatusId(categoryStatus);
                newsCategoryCollectionNewsCategory = em.merge(newsCategoryCollectionNewsCategory);
                if (oldCategoryStatusIdOfNewsCategoryCollectionNewsCategory != null) {
                    oldCategoryStatusIdOfNewsCategoryCollectionNewsCategory.getNewsCategoryCollection().remove(newsCategoryCollectionNewsCategory);
                    oldCategoryStatusIdOfNewsCategoryCollectionNewsCategory = em.merge(oldCategoryStatusIdOfNewsCategoryCollectionNewsCategory);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findCategoryStatus(categoryStatus.getCategoryStatusId()) != null) {
                throw new PreexistingEntityException("CategoryStatus " + categoryStatus + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CategoryStatus categoryStatus) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            CategoryStatus persistentCategoryStatus = em.find(CategoryStatus.class, categoryStatus.getCategoryStatusId());
            Collection<NewsCategory> newsCategoryCollectionOld = persistentCategoryStatus.getNewsCategoryCollection();
            Collection<NewsCategory> newsCategoryCollectionNew = categoryStatus.getNewsCategoryCollection();
            Collection<NewsCategory> attachedNewsCategoryCollectionNew = new ArrayList<NewsCategory>();
            for (NewsCategory newsCategoryCollectionNewNewsCategoryToAttach : newsCategoryCollectionNew) {
                newsCategoryCollectionNewNewsCategoryToAttach = em.getReference(newsCategoryCollectionNewNewsCategoryToAttach.getClass(), newsCategoryCollectionNewNewsCategoryToAttach.getCategoryId());
                attachedNewsCategoryCollectionNew.add(newsCategoryCollectionNewNewsCategoryToAttach);
            }
            newsCategoryCollectionNew = attachedNewsCategoryCollectionNew;
            categoryStatus.setNewsCategoryCollection(newsCategoryCollectionNew);
            categoryStatus = em.merge(categoryStatus);
            for (NewsCategory newsCategoryCollectionOldNewsCategory : newsCategoryCollectionOld) {
                if (!newsCategoryCollectionNew.contains(newsCategoryCollectionOldNewsCategory)) {
                    newsCategoryCollectionOldNewsCategory.setCategoryStatusId(null);
                    newsCategoryCollectionOldNewsCategory = em.merge(newsCategoryCollectionOldNewsCategory);
                }
            }
            for (NewsCategory newsCategoryCollectionNewNewsCategory : newsCategoryCollectionNew) {
                if (!newsCategoryCollectionOld.contains(newsCategoryCollectionNewNewsCategory)) {
                    CategoryStatus oldCategoryStatusIdOfNewsCategoryCollectionNewNewsCategory = newsCategoryCollectionNewNewsCategory.getCategoryStatusId();
                    newsCategoryCollectionNewNewsCategory.setCategoryStatusId(categoryStatus);
                    newsCategoryCollectionNewNewsCategory = em.merge(newsCategoryCollectionNewNewsCategory);
                    if (oldCategoryStatusIdOfNewsCategoryCollectionNewNewsCategory != null && !oldCategoryStatusIdOfNewsCategoryCollectionNewNewsCategory.equals(categoryStatus)) {
                        oldCategoryStatusIdOfNewsCategoryCollectionNewNewsCategory.getNewsCategoryCollection().remove(newsCategoryCollectionNewNewsCategory);
                        oldCategoryStatusIdOfNewsCategoryCollectionNewNewsCategory = em.merge(oldCategoryStatusIdOfNewsCategoryCollectionNewNewsCategory);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigDecimal id = categoryStatus.getCategoryStatusId();
                if (findCategoryStatus(id) == null) {
                    throw new NonexistentEntityException("The categoryStatus with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigDecimal id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            CategoryStatus categoryStatus;
            try {
                categoryStatus = em.getReference(CategoryStatus.class, id);
                categoryStatus.getCategoryStatusId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The categoryStatus with id " + id + " no longer exists.", enfe);
            }
            Collection<NewsCategory> newsCategoryCollection = categoryStatus.getNewsCategoryCollection();
            for (NewsCategory newsCategoryCollectionNewsCategory : newsCategoryCollection) {
                newsCategoryCollectionNewsCategory.setCategoryStatusId(null);
                newsCategoryCollectionNewsCategory = em.merge(newsCategoryCollectionNewsCategory);
            }
            em.remove(categoryStatus);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CategoryStatus> findCategoryStatusEntities() {
        return findCategoryStatusEntities(true, -1, -1);
    }

    public List<CategoryStatus> findCategoryStatusEntities(int maxResults, int firstResult) {
        return findCategoryStatusEntities(false, maxResults, firstResult);
    }

    private List<CategoryStatus> findCategoryStatusEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from CategoryStatus as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CategoryStatus findCategoryStatus(BigDecimal id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CategoryStatus.class, id);
        } finally {
            em.close();
        }
    }

    public int getCategoryStatusCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from CategoryStatus as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

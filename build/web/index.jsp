<%-- 
    Document   : index
    Created on : Mar 22, 2018, 1:36:07 PM
    Author     : oracle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <script src="jquery/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="js/test.js"></script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/main_page.css" rel="stylesheet" type="text/css"/>
        <title>News Shelf</title>
    </head>
    <body>
        <div class="header">
            <img class="img-head" src="images/header3.jpg">
        </div>
        
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
              <div class="navbar-header">
                <a class="navbar-brand" href="#">News Shelf</a>
              </div>
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">Add post</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
              </ul>
            </div>
          </nav>
        
        <div class="row">
            <div class="col-sm-2" ></div>
            <div id="content" class="col-sm-4" ></div>
            <div class="col-sm-2"></div>
        </div>
        
    </body>
    
</html>

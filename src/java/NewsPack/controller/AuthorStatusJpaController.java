/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack.controller;

import NewsPack.AuthorStatus;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import NewsPack.Authors;
import NewsPack.controller.exceptions.NonexistentEntityException;
import NewsPack.controller.exceptions.PreexistingEntityException;
import NewsPack.controller.exceptions.RollbackFailureException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author oracle
 */
public class AuthorStatusJpaController implements Serializable {

    public AuthorStatusJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(AuthorStatus authorStatus) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (authorStatus.getAuthorsCollection() == null) {
            authorStatus.setAuthorsCollection(new ArrayList<Authors>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<Authors> attachedAuthorsCollection = new ArrayList<Authors>();
            for (Authors authorsCollectionAuthorsToAttach : authorStatus.getAuthorsCollection()) {
                authorsCollectionAuthorsToAttach = em.getReference(authorsCollectionAuthorsToAttach.getClass(), authorsCollectionAuthorsToAttach.getAuthorId());
                attachedAuthorsCollection.add(authorsCollectionAuthorsToAttach);
            }
            authorStatus.setAuthorsCollection(attachedAuthorsCollection);
            em.persist(authorStatus);
            for (Authors authorsCollectionAuthors : authorStatus.getAuthorsCollection()) {
                AuthorStatus oldAuthorStatusIdOfAuthorsCollectionAuthors = authorsCollectionAuthors.getAuthorStatusId();
                authorsCollectionAuthors.setAuthorStatusId(authorStatus);
                authorsCollectionAuthors = em.merge(authorsCollectionAuthors);
                if (oldAuthorStatusIdOfAuthorsCollectionAuthors != null) {
                    oldAuthorStatusIdOfAuthorsCollectionAuthors.getAuthorsCollection().remove(authorsCollectionAuthors);
                    oldAuthorStatusIdOfAuthorsCollectionAuthors = em.merge(oldAuthorStatusIdOfAuthorsCollectionAuthors);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findAuthorStatus(authorStatus.getAuthorStatusId()) != null) {
                throw new PreexistingEntityException("AuthorStatus " + authorStatus + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(AuthorStatus authorStatus) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            AuthorStatus persistentAuthorStatus = em.find(AuthorStatus.class, authorStatus.getAuthorStatusId());
            Collection<Authors> authorsCollectionOld = persistentAuthorStatus.getAuthorsCollection();
            Collection<Authors> authorsCollectionNew = authorStatus.getAuthorsCollection();
            Collection<Authors> attachedAuthorsCollectionNew = new ArrayList<Authors>();
            for (Authors authorsCollectionNewAuthorsToAttach : authorsCollectionNew) {
                authorsCollectionNewAuthorsToAttach = em.getReference(authorsCollectionNewAuthorsToAttach.getClass(), authorsCollectionNewAuthorsToAttach.getAuthorId());
                attachedAuthorsCollectionNew.add(authorsCollectionNewAuthorsToAttach);
            }
            authorsCollectionNew = attachedAuthorsCollectionNew;
            authorStatus.setAuthorsCollection(authorsCollectionNew);
            authorStatus = em.merge(authorStatus);
            for (Authors authorsCollectionOldAuthors : authorsCollectionOld) {
                if (!authorsCollectionNew.contains(authorsCollectionOldAuthors)) {
                    authorsCollectionOldAuthors.setAuthorStatusId(null);
                    authorsCollectionOldAuthors = em.merge(authorsCollectionOldAuthors);
                }
            }
            for (Authors authorsCollectionNewAuthors : authorsCollectionNew) {
                if (!authorsCollectionOld.contains(authorsCollectionNewAuthors)) {
                    AuthorStatus oldAuthorStatusIdOfAuthorsCollectionNewAuthors = authorsCollectionNewAuthors.getAuthorStatusId();
                    authorsCollectionNewAuthors.setAuthorStatusId(authorStatus);
                    authorsCollectionNewAuthors = em.merge(authorsCollectionNewAuthors);
                    if (oldAuthorStatusIdOfAuthorsCollectionNewAuthors != null && !oldAuthorStatusIdOfAuthorsCollectionNewAuthors.equals(authorStatus)) {
                        oldAuthorStatusIdOfAuthorsCollectionNewAuthors.getAuthorsCollection().remove(authorsCollectionNewAuthors);
                        oldAuthorStatusIdOfAuthorsCollectionNewAuthors = em.merge(oldAuthorStatusIdOfAuthorsCollectionNewAuthors);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigDecimal id = authorStatus.getAuthorStatusId();
                if (findAuthorStatus(id) == null) {
                    throw new NonexistentEntityException("The authorStatus with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigDecimal id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            AuthorStatus authorStatus;
            try {
                authorStatus = em.getReference(AuthorStatus.class, id);
                authorStatus.getAuthorStatusId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The authorStatus with id " + id + " no longer exists.", enfe);
            }
            Collection<Authors> authorsCollection = authorStatus.getAuthorsCollection();
            for (Authors authorsCollectionAuthors : authorsCollection) {
                authorsCollectionAuthors.setAuthorStatusId(null);
                authorsCollectionAuthors = em.merge(authorsCollectionAuthors);
            }
            em.remove(authorStatus);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<AuthorStatus> findAuthorStatusEntities() {
        return findAuthorStatusEntities(true, -1, -1);
    }

    public List<AuthorStatus> findAuthorStatusEntities(int maxResults, int firstResult) {
        return findAuthorStatusEntities(false, maxResults, firstResult);
    }

    private List<AuthorStatus> findAuthorStatusEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from AuthorStatus as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public AuthorStatus findAuthorStatus(BigDecimal id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(AuthorStatus.class, id);
        } finally {
            em.close();
        }
    }

    public int getAuthorStatusCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from AuthorStatus as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

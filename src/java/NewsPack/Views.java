/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsPack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author oracle
 */
@Entity
@Table(name = "VIEWS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Views.findAll", query = "SELECT v FROM Views v"),
    @NamedQuery(name = "Views.findByViewId", query = "SELECT v FROM Views v WHERE v.viewId = :viewId"),
    @NamedQuery(name = "Views.findByBookmarks", query = "SELECT v FROM Views v WHERE v.bookmarks = :bookmarks"),
    @NamedQuery(name = "Views.findByTweets", query = "SELECT v FROM Views v WHERE v.tweets = :tweets"),
    @NamedQuery(name = "Views.findByFacebookShares", query = "SELECT v FROM Views v WHERE v.facebookShares = :facebookShares")})
public class Views implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "VIEW_ID")
    private BigDecimal viewId;
    @Column(name = "BOOKMARKS")
    private BigInteger bookmarks;
    @Column(name = "TWEETS")
    private BigInteger tweets;
    @Column(name = "FACEBOOK_SHARES")
    private BigInteger facebookShares;
    @OneToMany(mappedBy = "viewId")
    private Collection<News> newsCollection;

    public Views() {
    }

    public Views(BigDecimal viewId) {
        this.viewId = viewId;
    }

    public BigDecimal getViewId() {
        return viewId;
    }

    public void setViewId(BigDecimal viewId) {
        this.viewId = viewId;
    }

    public BigInteger getBookmarks() {
        return bookmarks;
    }

    public void setBookmarks(BigInteger bookmarks) {
        this.bookmarks = bookmarks;
    }

    public BigInteger getTweets() {
        return tweets;
    }

    public void setTweets(BigInteger tweets) {
        this.tweets = tweets;
    }

    public BigInteger getFacebookShares() {
        return facebookShares;
    }

    public void setFacebookShares(BigInteger facebookShares) {
        this.facebookShares = facebookShares;
    }

    @XmlTransient
    public Collection<News> getNewsCollection() {
        return newsCollection;
    }

    public void setNewsCollection(Collection<News> newsCollection) {
        this.newsCollection = newsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (viewId != null ? viewId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Views)) {
            return false;
        }
        Views other = (Views) object;
        if ((this.viewId == null && other.viewId != null) || (this.viewId != null && !this.viewId.equals(other.viewId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NewsPack.Views[ viewId=" + viewId + " ]";
    }
    
}
